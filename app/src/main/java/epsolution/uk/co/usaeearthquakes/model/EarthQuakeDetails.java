package epsolution.uk.co.usaeearthquakes.model;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class EarthQuakeDetails {
    private String type;
    private Properties properties;

    public EarthQuakeDetails() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
