package epsolution.uk.co.usaeearthquakes.utils;

import java.util.Random;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class Constants {

    public static int randomInt(int max, int min){
        return new Random().nextInt(max - min) + min;
    }

}
