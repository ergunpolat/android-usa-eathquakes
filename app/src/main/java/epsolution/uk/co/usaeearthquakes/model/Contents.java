package epsolution.uk.co.usaeearthquakes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class Contents {

    @SerializedName("geoserve.json")
    @Expose
    private GeoserveJson geoserveJson;

    public Contents() {
    }

    public GeoserveJson getGeoserveJson() {
        return geoserveJson;
    }

    public void setGeoserveJson(GeoserveJson geoserveJson) {
        this.geoserveJson = geoserveJson;
    }
}
