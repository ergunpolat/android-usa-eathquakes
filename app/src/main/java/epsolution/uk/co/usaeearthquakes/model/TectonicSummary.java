package epsolution.uk.co.usaeearthquakes.model;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class TectonicSummary {
    private String text;

    public TectonicSummary() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
