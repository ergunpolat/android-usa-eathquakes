package epsolution.uk.co.usaeearthquakes;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import epsolution.uk.co.usaeearthquakes.activities.QuakesListActivity;
import epsolution.uk.co.usaeearthquakes.model.CitiesDetails;
import epsolution.uk.co.usaeearthquakes.model.City;
import epsolution.uk.co.usaeearthquakes.model.Contents;
import epsolution.uk.co.usaeearthquakes.model.EarthQuake;
import epsolution.uk.co.usaeearthquakes.model.EarthQuakeDetails;
import epsolution.uk.co.usaeearthquakes.model.EarthQuakeParsed;
import epsolution.uk.co.usaeearthquakes.model.Features;
import epsolution.uk.co.usaeearthquakes.model.Geometry;
import epsolution.uk.co.usaeearthquakes.model.Geoserve;
import epsolution.uk.co.usaeearthquakes.model.GeoserveJson;
import epsolution.uk.co.usaeearthquakes.model.Products;
import epsolution.uk.co.usaeearthquakes.model.TectonicSummary;
import epsolution.uk.co.usaeearthquakes.ui.CustomInfoWindow;
import epsolution.uk.co.usaeearthquakes.utils.ApiClient;
import epsolution.uk.co.usaeearthquakes.service.ApiInterface;
import epsolution.uk.co.usaeearthquakes.utils.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener, GoogleMap.OnMarkerClickListener {

    private static final String TAG = MapsActivity.class.getSimpleName();

    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private ApiInterface apiService;

    private AlertDialog.Builder dialogBuilder;
    private AlertDialog dialog;

    private BitmapDescriptor[] iconColor;


    private Button showListBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        showListBtn = (Button) findViewById(R.id.showListBtn);

        showListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MapsActivity.this, QuakesListActivity.class));
            }
        });

        apiService = ApiClient.getClient().create(ApiInterface.class);
        iconColor = new BitmapDescriptor[]{
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE),
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW),
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_CYAN),
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA),
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE),
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN),
                BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)
        };



        getEarthQuakes();


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }


    public void getEarthQuakes() {

        final EarthQuakeParsed earthQuake = new EarthQuakeParsed();

        Call<EarthQuake> earthQuakeCall = apiService.getHourlySummary();

        earthQuakeCall.enqueue(new Callback<EarthQuake>() {
            @Override
            public void onResponse(@NonNull Call<EarthQuake> call, @NonNull Response<EarthQuake> response) {

                for (Features features : response.body().getFeatures()) {
                    epsolution.uk.co.usaeearthquakes.model.Properties properties =
                            features.getProperties();

                    Geometry geometry = features.getGeometry();
                    List<Double> coo = geometry.getCoordinates();

                    double lon = coo.get(0);
                    double lat = coo.get(1);

                    earthQuake.setId(features.getId());
                    earthQuake.setPlace(properties.getPlace());
                    earthQuake.setMag(properties.getMag());

                    earthQuake.setType(properties.getType());

                    java.text.DateFormat dateFormat = java.text.DateFormat.getDateTimeInstance();
                    String formattedDate = dateFormat.format(new Date(Long.valueOf(properties.getTime())).getTime());

                    earthQuake.setTime(formattedDate);
                    earthQuake.setLog(lon);
                    earthQuake.setLat(lat);

                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.icon(iconColor[Constants.randomInt(iconColor.length, 0)]);
                    markerOptions.title(earthQuake.getPlace());
                    markerOptions.position(new LatLng(lat, lon));
                    markerOptions.snippet("Mag: " + earthQuake.getMag() + "\n" + "Date: " + formattedDate);


                    if (earthQuake.getMag() >= 2.0){
                        CircleOptions circleOptions = new CircleOptions();
                        circleOptions.center(new LatLng(earthQuake.getLat(), earthQuake.getLog()));
                        circleOptions.radius(30000);
                        circleOptions.strokeWidth(3.6f);
                        circleOptions.fillColor(Color.RED);
                        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
                    mMap.addCircle(circleOptions);
                    }
                    final Marker marker = mMap.addMarker(markerOptions);


                    Call<EarthQuakeDetails> earthQuakeDetailsCall =
                            apiService.getEarthQuakeDetails(earthQuake.getId());

                    earthQuakeDetailsCall.enqueue(new Callback<EarthQuakeDetails>() {
                        @Override
                        public void onResponse(Call<EarthQuakeDetails> call, Response<EarthQuakeDetails> response) {
                            epsolution.uk.co.usaeearthquakes.model.Properties detailProperties = response.body().getProperties();

                            Products products = detailProperties.getProducts();
                            if(products.getGeoserve() != null) {
                                for (Geoserve geoserve : products.getGeoserve()) {
                                    Contents contents = geoserve.getContents();
                                    GeoserveJson geoserveJson = contents.getGeoserveJson();

                                    marker.setTag(geoserveJson.getUrl());
                                }
                            }

                        }

                        @Override
                        public void onFailure(Call<EarthQuakeDetails> call, Throwable t) {
                            Log.e(TAG, t.toString());
                        }
                    });


                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 1));


                }
            }

            @Override
            public void onFailure(@NonNull Call<EarthQuake> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setInfoWindowAdapter(new CustomInfoWindow(getApplicationContext()));
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerClickListener(this);

        mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d(TAG, location.toString());
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        if (Build.VERSION.SDK_INT < 23) {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                    0, mLocationListener);
        } else {

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);

            } else {
                mLocationManager
                        .requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                                0, mLocationListener);
                Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);

            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0,
                        0, mLocationListener);
            }
        }

    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        getEarthQuakeDetails(marker.getTag().toString());
    }

    private void getEarthQuakeDetails(String detailsUrl) {

        Call<CitiesDetails> getCitiesDetails = apiService.getCityDetails(detailsUrl);

        getCitiesDetails.enqueue(new Callback<CitiesDetails>() {
            @Override
            public void onResponse(Call<CitiesDetails> call, Response<CitiesDetails> response) {


                dialogBuilder = new AlertDialog.Builder(MapsActivity.this);
                View view = getLayoutInflater().inflate(R.layout.popup, null);

                Button dismissBtn = (Button) view.findViewById(R.id.dismissPop);
                Button dismissBtnTop = (Button) view.findViewById(R.id.dismissPopup);
                TextView popList = (TextView) view.findViewById(R.id.popList);
                WebView htmlPop = (WebView) view.findViewById(R.id.htmlWebView);
                StringBuilder stringBuilder = new StringBuilder();


                for (City cities : response.body().getCities()) {
                    stringBuilder.append("City: ")
                            .append(cities.getName())
                            .append("\n").append("Distance: ")
                            .append(cities.getDistance())
                            .append("\n").append("Population: ")
                            .append(cities.getPopulation())
                            .append("\n")
                            .append("\n");
                }

                if (response.body().getTectonicSummary() != null) {
                    TectonicSummary tectonicSummary = response.body().getTectonicSummary();
                    if (tectonicSummary.getText() != null) {

                        String text = tectonicSummary.getText();
                        htmlPop.loadDataWithBaseURL(null,text,"text/html","UTF-8",null);
                    }
                }


                popList.setText(stringBuilder);
                dismissBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                dismissBtnTop.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });
                dialogBuilder.setView(view);
                dialog = dialogBuilder.create();
                dialog.show();

            }

            @Override
            public void onFailure(Call<CitiesDetails> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }
}
