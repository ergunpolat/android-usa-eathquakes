package epsolution.uk.co.usaeearthquakes.model;

import java.util.List;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class CitiesDetails {

    private List<City> cities = null;
    private TectonicSummary tectonicSummary;

    public CitiesDetails() {
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
    }

    public TectonicSummary getTectonicSummary() {
        return tectonicSummary;
    }

    public void setTectonicSummary(TectonicSummary tectonicSummary) {
        this.tectonicSummary = tectonicSummary;
    }
}
