package epsolution.uk.co.usaeearthquakes.model;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class GeoserveJson {

    private String url;

    public GeoserveJson() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
