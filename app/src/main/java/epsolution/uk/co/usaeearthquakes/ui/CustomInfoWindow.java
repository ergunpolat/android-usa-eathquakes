package epsolution.uk.co.usaeearthquakes.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import epsolution.uk.co.usaeearthquakes.R;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class CustomInfoWindow implements GoogleMap.InfoWindowAdapter {
    private View mView;
    private LayoutInflater mLayoutInflater;
    private Context mContext;


    public CustomInfoWindow(Context context) {
      this.mContext = context;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mView = mLayoutInflater.inflate(R.layout.custom_info_window, null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {

        TextView title = (TextView) mView.findViewById(R.id.winTitle);
        title.setText(marker.getTitle());


        TextView mag = (TextView) mView.findViewById(R.id.mag);
        mag.setText(marker.getSnippet());

        Button mButton = (Button) mView.findViewById(R.id.btn);


        return mView;
    }
}
