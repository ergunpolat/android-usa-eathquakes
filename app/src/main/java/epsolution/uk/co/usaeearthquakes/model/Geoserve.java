package epsolution.uk.co.usaeearthquakes.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class Geoserve {

    @SerializedName("contents")
    @Expose
    private Contents contents;

    public Geoserve() {
    }

    public Contents getContents() {
        return contents;
    }

    public void setContents(Contents contents) {
        this.contents = contents;
    }
}
