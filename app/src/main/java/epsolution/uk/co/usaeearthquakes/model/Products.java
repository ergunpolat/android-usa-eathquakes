package epsolution.uk.co.usaeearthquakes.model;

import java.util.List;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class Products {

    private List<Geoserve> geoserve;

    public Products() {
    }

    public List<Geoserve> getGeoserve() {
        return geoserve;
    }

    public void setGeoserve(List<Geoserve> geoserve) {
        this.geoserve = geoserve;
    }
}
