package epsolution.uk.co.usaeearthquakes.model;

import java.util.List;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public class EarthQuake {
    private String type;
    private List<Features> features;

    public EarthQuake() {
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Features> getFeatures() {
        return features;
    }

    public void setFeatures(List<Features> features) {
        this.features = features;
    }
}
