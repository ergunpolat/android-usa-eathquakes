package epsolution.uk.co.usaeearthquakes.service;

import epsolution.uk.co.usaeearthquakes.model.CitiesDetails;
import epsolution.uk.co.usaeearthquakes.model.EarthQuake;
import epsolution.uk.co.usaeearthquakes.model.EarthQuakeDetails;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by Ergun Polat on 03/09/2017.
 */

public interface ApiInterface {


    @GET("summary/all_hour.geojson")
    Call<EarthQuake> getHourlySummary();

    @GET("detail/{id}.geojson")
    Call<EarthQuakeDetails> getEarthQuakeDetails(@Path("id") String id);

    @GET
    Call<CitiesDetails> getCityDetails(@Url String cityUrl);

}
