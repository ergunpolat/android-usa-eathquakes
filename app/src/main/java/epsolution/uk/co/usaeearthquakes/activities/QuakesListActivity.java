package epsolution.uk.co.usaeearthquakes.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import epsolution.uk.co.usaeearthquakes.R;
import epsolution.uk.co.usaeearthquakes.model.EarthQuake;
import epsolution.uk.co.usaeearthquakes.model.EarthQuakeParsed;
import epsolution.uk.co.usaeearthquakes.model.Features;
import epsolution.uk.co.usaeearthquakes.model.Geometry;
import epsolution.uk.co.usaeearthquakes.service.ApiInterface;
import epsolution.uk.co.usaeearthquakes.utils.ApiClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuakesListActivity extends AppCompatActivity {

    private static final String TAG = QuakesListActivity.class.getSimpleName();

    private List<String> quakesList;
    private ArrayAdapter arrayAdapter;
    private List<EarthQuakeParsed> mEarthQuakeParseds;

    private ApiInterface apiService;


    private ListView mListView;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quakes_list);

        mListView = (ListView) findViewById(R.id.placesListView);
        quakesList = new ArrayList<>();
        mEarthQuakeParseds = new ArrayList<>();

        apiService = ApiClient.getClient().create(ApiInterface.class);

        getEarthQuakes();
    }


    private void getEarthQuakes(){
        final EarthQuakeParsed earthQuake = new EarthQuakeParsed();

        Call<EarthQuake> earthQuakeCall = apiService.getHourlySummary();

        earthQuakeCall.enqueue(new Callback<EarthQuake>() {
            @Override
            public void onResponse(Call<EarthQuake> call, Response<EarthQuake> response) {

                for (Features features : response.body().getFeatures()) {
                    epsolution.uk.co.usaeearthquakes.model.Properties properties =
                            features.getProperties();

                    Geometry geometry = features.getGeometry();
                    List<Double> coo = geometry.getCoordinates();

                    double lon = coo.get(0);
                    double lat = coo.get(1);

                    earthQuake.setId(features.getId());
                    earthQuake.setPlace(properties.getPlace());
                    earthQuake.setMag(properties.getMag());

                    earthQuake.setType(properties.getType());

                    java.text.DateFormat dateFormat = java.text.DateFormat.getDateTimeInstance();
                    String formattedDate = dateFormat.format(new Date(Long.valueOf(properties.getTime())).getTime());

                    earthQuake.setTime(formattedDate);
                    earthQuake.setLog(lon);
                    earthQuake.setLat(lat);

                    quakesList.add(earthQuake.getPlace());
                }

                arrayAdapter = new ArrayAdapter(QuakesListActivity.this,
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1, quakesList);
                mListView.setAdapter(arrayAdapter);
                arrayAdapter.notifyDataSetChanged();
            }
            @Override
            public void onFailure(Call<EarthQuake> call, Throwable t) {
                Log.e(TAG, t.toString());
            }
        });


    }
}
